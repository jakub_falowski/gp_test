import {useEffect} from 'react';
import L from 'leaflet';

export default function DiviseGrid() {
    useEffect(() =>{
      const map = L.map('diviseMap', {
          center: [50.029054, 22.034131],
          zoom: 10,
      });

      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: 'test'}).addTo(map);

      const tiles:any = new L.GridLayer();
      tiles.createTile = function(coords:any) {
        const tile = L.DomUtil.create('canvas', 'leaflet-tile');
        const ctx = tile.getContext('2d');
        const size = this.getTileSize()
        tile.width = size.x
        tile.height = size.y
        
        // calculate projection coordinates of top left tile pixel
        const nwPoint = coords.scaleBy(size)
        
        // calculate geographic coordinates of top left tile pixel
        const nw = map.unproject(nwPoint, coords.z)
        
        if(ctx){
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, size.x, 50);
          ctx.fillStyle = 'black';
          ctx.fillText('x: ' + coords.x + ', y: ' + coords.y + ', zoom: ' + coords.z, 20, 20);
          ctx.fillText('lat: ' + nw.lat + ', lon: ' + nw.lng, 20, 40);
          ctx.strokeStyle = 'red';
          ctx.beginPath();
          ctx.moveTo(0, 0);
          ctx.lineTo(size.x-1, 0);
          ctx.lineTo(size.x-1, size.y-1);
          ctx.lineTo(0, size.y-1);
          ctx.closePath();
          ctx.stroke();
        }
        
        return tile;
      }
      tiles.addTo(map)
  })
    return(
    <div>
      <h1>Dzielenie siatką geometryczną obiektów</h1>
      <div id='diviseMap' style={{height: "30vh", width: "30vw"}}></div>
    </div>
    )
}