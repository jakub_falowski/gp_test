import {useEffect} from 'react';
import 'leaflet';
import  './L.KML';
import windyKML from './windy.kml';
declare let L:any;

export default function KMLtype() {
  useEffect(()=>{
    const map = L.map('KMLMap', {
      center: [38.100079, 14.752057],
      zoom: 10,
    });

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: 'test'}).addTo(map);

    const track = new L.KML(windyKML, {async: true}).on('loaded', function (e:any) {
			map.fitBounds(e.target.getBounds());
		})
		.addTo(map);
	  L.control.layers({}, {'Track': track}, {collapsed: false}).addTo(map);
  })
    
  return (
    <>
      <h1>KML</h1>
      <div id="KMLMap" style={{height: "30vh", width: "30vw"}}></div>
    </>
  );
}
