import {useEffect} from 'react';
import L from 'leaflet';

export default function WFS(){
    useEffect(()=>{
        const map = L.map('WFSMap', {
            center: [-73.999079,40.732188],
            zoom: 2,
        });
  
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: 'test'}).addTo(map);

        const wfs_url = "http://localhost:8080/geoserver/wfs?service=wfs&version=2.0.0&request=GetFeature&typeNames=tiger:tiger_roads&outputFormat=application/json&count=200";

        const geojsonStyle = {
            fillColor: "#ff7800",
            color: "#000",
            weight: 5,
            opacity: 1,
            fillOpacity: 0.8
        }

        fetch(wfs_url).then(response => response.json()).then(res => {
            const layer = L.geoJSON(res, {
            onEachFeature: function(f, l) {
                l.bindPopup(f.properties.NAME);
            },
            style: geojsonStyle,
            });
            layer.addTo(map);
            map.fitBounds(layer.getBounds());
        });
    })
    
    return(
        <>
            <h1>Warstwy WFS</h1>
            <div id="WFSMap" style={{height: "30vh", width: "30vw"}}> </div>
        </>
    
    )
}