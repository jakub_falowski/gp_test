import { MapContainer, TileLayer, WMSTileLayer } from 'react-leaflet';

export default function WMS(){
    return (
        <>
            <h1>Warstwy WMS</h1>
            <MapContainer center={[35.502976, -77.465687]} zoom={5} style={{height:"30vh", width:"30vw"}}>
            <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" attribution='test' />
            <WMSTileLayer
                url="http://mesonet.agron.iastate.edu/cgi-bin/wms/nexrad/n0r.cgi"
                layers="nexrad-n0r-900913"
                format="image/png"
                transparent={true}
            />
            </MapContainer>
        </>
      );
}