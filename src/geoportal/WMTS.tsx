import React, {useEffect} from 'react';
import 'leaflet';
import "./leaflet.tilelayer.wmts.min";
declare let L:any;

export default function WMTS(){
    useEffect(() =>{
        const map = L.map('WMTSMap', {
            center: [50.029427, 22.013689],
            zoom: 3,
        });
  
        const url = 'http://e.s2maps-tiles.eu/wmts';
                
        const options = {
            tileMatrixSet:'g',
            layer:'bluemarble_3857',
        };

        const wmtsLayer = L.tileLayer.wmts(url, options);

        let baseLayers:{ [key: string]: string } = {};
        baseLayers['Blue Marble World'] = wmtsLayer;
        
        wmtsLayer.addTo(map);
        L.control.layers(baseLayers).addTo(map);
    })

    return(
        <div>
          <h1>Warstwy WMTS</h1>
          <div id='WMTSMap' style={{height: "30vh", width: "30vw"}}></div>
        </div>
    )
}
