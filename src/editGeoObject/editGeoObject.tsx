import { MapContainer, TileLayer, FeatureGroup,  } from 'react-leaflet';
import { EditControl } from 'react-leaflet-draw';
import "leaflet/dist/leaflet.css";
import "leaflet-draw/dist/leaflet.draw.css";

export default function EditGeoObject(){

    const _onCreate = (e:any) => {
        console.log(e)
    }

    const _onEdited = (e:any) => {
        console.log(e)
    }

    const _onDeleted = (e:any) => {
        console.log(e)
    }
  return (
    <>
    <h1>Edytowanie obiektów geometrycznych</h1>
        <MapContainer center={[50.028142, 22.019007]} zoom={20} style={{height:"30vh", width:"30vw"}}>
        <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" attribution='test' />
        <FeatureGroup>
            <EditControl
            position='topright'
            onCreated={_onCreate}
            onEdited={_onEdited}
            onDeleted={_onDeleted}
            draw={{
                polyline: true,
                polygon: true,
                circle: true,
                rectangle: true
            }}
            />
        </FeatureGroup>
        </MapContainer>
        <h1>Dzielenie linią obiektów geometrycznych</h1>
        <iframe width="100%" height="300" src="//jsfiddle.net/falo99/8s7xnyqr/embedded/" title='divise line geoobject' />
    </>
  );
};
