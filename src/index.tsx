import React from 'react';
import { createRoot } from "react-dom/client";
import AddGeoObject from './addGeoObject/addGeoObject';
import EditGeoObject from './editGeoObject/editGeoObject';
import DiviseGrid from './diviseGridGeoObject/diviseGrid';
import ImageContour from './imageContour/imageContour';
import WMS from './geoportal/WMS';
import WMTS from './geoportal/WMTS';
import WFS from './geoportal/WFS';
import KMLtype from './KML/KMLtype';
import SHP from './SHP/SHP';
import ElevationData from './elevationData/elevationData';

const rootElement = document.getElementById("root");
if (!rootElement) throw new Error('Failed to find the root element');
const root = createRoot(rootElement);
root.render(
  <>
    <AddGeoObject />
    <EditGeoObject />
    <DiviseGrid />
    <ImageContour />
    <WMS />
    <WMTS />
    <WFS />
    <KMLtype />
    <SHP />
    {/* <ElevationData /> */}
    <h1>Demo danych wysokościowych</h1>
    <iframe src="https://raruto.github.io/leaflet-elevation/examples/leaflet-elevation_geojson-data.html" title="Dane wysokościowe" width="600px" height="600px" />
    <h1>Demo pobierania obrysów</h1>
    <iframe src="http://mapbox.github.io/leaflet-image/" title="Pobieranie obrysów" width="600px" height="600px" />
    
  </>
);

