import L from "leaflet";
import { MapContainer, TileLayer, Polygon, Marker, Popup, FeatureGroup, Circle, Rectangle} from "react-leaflet"
import 'leaflet/dist/leaflet.css'
import { statesData } from "./statesData";

export default function AddGeoObject() {

  const markerIcon = new L.Icon({
    iconUrl: require("./iconBa.png"),
    iconSize: [45,45]
  })

  return (
    <>
      <h1>Dodawanie obiektów geometrycznych</h1>
      <MapContainer center={[33.82, -111.75]} zoom={5} style={{height:"30vh", width:"30vw"}}>
        <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" attribution='test' />
        {
          statesData.features.map((state) => {
            const coordinates: any = state.geometry.coordinates[0].map((item)=> [item[1], item[0]]);
            return (<Polygon 
              key={state.id}
              pathOptions={{
                fillColor:'#888888',
                fillOpacity: 0.5,
                weight: 2,
                opacity: 1,
                dashArray: [3,4],
                color: 'white'
              }}
              positions={coordinates}
              eventHandlers={{
                mouseover: (e) =>{
                  const layer = e.target;
                  layer.setStyle({
                    fillOpacity: 0.7,
                    fillColor: '#FFFFFF'
                  })
                },
                mouseout: (e) =>{
                  const layer = e.target;
                  layer.setStyle({
                    fillOpacity: 0.5,
                    fillColor:'#888888'
                  })
                },
                click: (e) =>{
                  const layer = e.target;
                  layer.setStyle({
                    fillOpacity: 1,
                    fillColor:'#FFFFFF',
                    dashArray: "3"
                  })
                }
              }}
              />)
          })
        }
        <Marker position={[33.82, -111.75]} icon={markerIcon} >
          <Popup>
            <b>Marker</b><p>Własny wzór</p>
          </Popup>
        </Marker>
        <FeatureGroup>
          <Popup>Jedna z działek Jakuba Kowalskiego </Popup>
          <Circle center={[33.03, -108.90]} radius={20000} />
          <Rectangle bounds={[[30, -107.00],[31, -107.56]]} />
        </FeatureGroup>

    </MapContainer>
  </>
  );
}
