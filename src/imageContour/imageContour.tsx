import {useEffect} from 'react';
import L from 'leaflet';

export default function ImageContour(){
    useEffect(() =>{
        const map = L.map('contourMap', {
            center: [50.029427, 22.013689],
            zoom: 16,
        });
  
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: 'test'}).addTo(map);
  
        const imageUrl = 'https://maps.lib.utexas.edu/maps/historical/newark_nj_1922.jpg',
        imageBounds:any = [[50.031274, 22.013088], [50.028848, 22.016403]];
        L.imageOverlay(imageUrl, imageBounds).addTo(map);
    })

    return(
        <div>
          <h1>Dodawanie obrysów działek za pomocą zdjęć</h1>
          <div id='contourMap' style={{height: "30vh", width: "30vw"}}></div>
        </div>
    )
}
