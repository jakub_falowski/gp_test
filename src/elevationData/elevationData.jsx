import L from 'leaflet';
import { useEffect } from 'react';
import * as elevation from 'https://unpkg.com/@raruto/leaflet-elevation@2.2.8/dist/leaflet-elevation.min.js';
import url from "./demo.geojson"

export default function ElevationData(){
	let opts = {
		map: {
			center: [41.4583, 12.7059],
			zoom: 5,
			fullscreenControl: false,
			resizerControl: true,
			preferCanvas: true,
			rotate: true,
			// bearing: 45,
			rotateControl: {
				closeOnZeroBearing: true
			},
		},
		elevationControl: {
			url: url,
			options: {
				theme: "lime-theme",
				collapsed: false,
				autohide: false,
				autofitBounds: true,
				position: "bottomleft",
				detached: true,
				summary: "inline",
				imperial: false,
				// altitude: "disabled",
				slope: true,
				speed: false,
				acceleration: false,
				time: true,
				legend: true,
				followMarker: true,
				almostOver: true,
				distanceMarkers: true,
				hotline: false,
			},
		},
		layersControl: {
			options: {
				collapsed: false,
			},
		},
	};

	useEffect(()=>{
        const map = L.map('elevationMap', opts.map);
  
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: 'test'}).addTo(map);

		let controlElevation = L.control.elevation(opts.elevationControl.options).addTo(map);
		let controlLayer = L.control.layers(null, null, opts.layersControl.options);
		controlElevation.on('eledata_loaded', ({layer, name}) => controlLayer.addTo(map) && layer.eachLayer((trkseg) => trkseg.feature.geometry.type !== "Point" && controlLayer.addOverlay(trkseg, trkseg.feature && trkseg.feature.properties) && name));

		console.log(controlElevation);
		controlElevation.load(opts.elevationControl.url);
	})
    return (
        <>
            <h1>Dane wysokościowe</h1>
			<div id="elevationMap" className='leaflet-map' style={{height: "30vh", width: "30vw"}}> </div>
        </>
      );
}