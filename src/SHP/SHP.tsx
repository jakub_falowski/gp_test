import {useEffect} from 'react';
import 'leaflet';
import './leaflet.shpfile';
import shapefilezip from './congress.zip';
declare let L:any;

export default function SHP(){
    useEffect(()=>{
        const map = L.map('SHPMap', {
            center: [42.256940, -71.798674],
            zoom: 10,
        });
  
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: 'test'}).addTo(map);

        const shpfile = new L.Shapefile(shapefilezip, {
			onEachFeature: function(feature:any, layer:any) {
                
				if (feature.properties) {
					layer.bindPopup(Object.keys(feature.properties).map(function(k) {
						return k + ": " + feature.properties[k];
					}).join("<br />"), {
						maxHeight: 200
					});
				}
			}
		});

		shpfile.addTo(map);
		// shpfile.once("data:loaded", function() {
		// 	console.log("finished loaded shapefile");
		// });
    })
    return (
        <>
            <h1>Warstwy SHP</h1>
            <div id="SHPMap" style={{height: "30vh", width: "30vw"}}> </div>
        </>
      );
}